The goal of this project is to showcase a simple but complete example of jdo persistence with a custom, 2 field ID along with a Collection.
The database for this project is h2.

## Installation
- Have java 19, maven and git ready
- clone the repository
```cmd 
 git clone https://gitlab.com/dechoudens/dntest.git)
```
- clean and install with maven
```cmd 
 mvn clean install
```
- start the newly created jar
```cmd 
 java -jar target/dntest-1.0.jar
```

## Enhance  : 
Note that the plugin datanucleus-maven-plugin takes care of enhancing the classes for the created jar.

This command is used for enhancing in development :
```cmd 
 mvn datanucleus:enhance
```


### Optionnal : how to access h2 db

- Install h2 (either by zip or .exe) : https://www.h2database.com/html/main.html
- Go to the /bin and start the h2 jar:
```cmd 
 java -jar h2-*.jar)
```
or use the h2.bat
- On the newly opened browser, press 'Connect'
