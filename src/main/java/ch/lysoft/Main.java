package ch.lysoft;

import ch.lysoft.jdo.Inventory;
import ch.lysoft.jdo.Product;

import javax.jdo.*;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.execute();
    }

    private void execute() {
        PersistenceManagerFactory pmf = JDOHelper.getPersistenceManagerFactory("DB1");
        PersistenceManager pm = pmf.getPersistenceManager();

        try (pm) {
            Transaction tx = pm.currentTransaction();
            tx.begin();
            Inventory inv = new Inventory("2", "9", "My Inventory");
            List<Object> products = inv.getProducts();
            products.add(new Product("1", "1", "Product 1", "The first product", 39.99));
            products.add(new Product("1", "2", "Product 2", "The second product", 429.99));
            products.add(new Product("1", "3", "Product 3", "The third product", 339.99));
            products.add(new Product("1", "4", "Product 4", "The fourth product", 559.99));
            products.add(new Product("1", "5", "Product 5", "The fifth product", 19.99));
            products.add(new Product("1", "6", "Product 6", "The sixth product", 229.99));
            products.add(new Product("1", "7", "Product 7", "The seventh product", 433.99));
            products.add(new Product("1", "8", "Product 8", "The eighth product", 413.99));
            pm.makePersistent(inv);
            tx.commit();

            tx.begin();
            Query<Inventory> inventoryQuery = pm.newQuery(Inventory.class);
            for (Inventory inventory : inventoryQuery.executeList()) {
                System.out.println(inventory);
            }
            tx.commit();
        }
    }
}