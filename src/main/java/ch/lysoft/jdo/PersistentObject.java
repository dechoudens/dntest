package ch.lysoft.jdo;

public abstract class PersistentObject {
    public String cid;
    public String oid;

    public PersistentObject(String cid, String oid) {
        this.cid = cid;
        this.oid = oid;
    }
}
