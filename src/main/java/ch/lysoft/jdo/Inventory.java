package ch.lysoft.jdo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Inventory extends PersistentObject{
    private String name;
    private List<Object> products;

    public Inventory(String cid, String oid, String name) {
        super(cid, oid);
        this.name = name;
        this.products = new ArrayList();
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "\n\tcid='" + cid + '\'' +
                "\n\toid='" + oid + '\'' +
                "\n\tname='" + name + '\'' +
                "\n\tproducts=" + products +
                "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Inventory inventory = (Inventory) o;
        return Objects.equals(name, inventory.name) && Objects.equals(products, inventory.products);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, products);
    }

    public List<Object> getProducts() {
        return products;
    }

}
