package ch.lysoft.jdo;

import java.io.Serializable;
import java.util.Objects;
import java.util.StringTokenizer;

public class ObjectId implements Serializable {
    public String cid;
    public String oid;

    public ObjectId(){}

    public ObjectId(String value){
        StringTokenizer token = new StringTokenizer (value, "::");
        token.nextToken();
        this.cid = token.nextToken();
        this.oid = token.nextToken();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectId objectId = (ObjectId) o;
        return Objects.equals(cid, objectId.cid) && Objects.equals(oid, objectId.oid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cid, oid);
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "::"  + this.cid + "::" + this.oid;
    }
}
