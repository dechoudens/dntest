package ch.lysoft.jdo;

import java.util.Objects;

public class Product extends PersistentObject{
    private String name;
    private String description;
    private double price;

    public Product(String cid, String oid, String name, String description, double price) {
        super(cid, oid);
        this.name = name;
        this.description = description;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0 && Objects.equals(name, product.name) && Objects.equals(description, product.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, price);
    }

    @Override
    public String toString() {
        return "Product{" +
                "\n\tcid='" + cid + '\'' +
                "\n\toid='" + oid + '\'' +
                "\n\tname='" + name + '\'' +
                "\n\tdescription='" + description + '\'' +
                "\n\tprice=" + price +
                "\n}";
    }
}
